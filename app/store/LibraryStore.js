/**
 * Created by dima on 10/12/14.
 */
Ext.define('BookApp.store.LibraryStore', {
    extend: 'Ext.data.Store',
    model: 'BookApp.model.Library',
    autoLoad: true,
    storeId: 'LibraryStore',
    proxy: {
        type: 'ajax',
        url: 'app/data/all.php',
        reader: {
            type: 'json',
            root: 'library',
            successProperty: 'success'
        }
    }
});
/**
 * Created by dima on 10/12/14.
 */
Ext.define('BookApp.store.BookStore', {
    extend: 'Ext.data.Store',
    requires  : ['BookApp.model.Book', 'Ext.data.proxy.Ajax'],
    model: 'BookApp.model.Book',
    autoLoad: false,
    storeId: 'BookStore',
    proxy: {
        type: 'ajax',
        url: 'app/data/all.php',
        reader: {
            type: 'json',
            root: 'books',
            successProperty: 'success'
        }
    }
    //proxy: new Ext.data.HttpProxy({
    //    url: 'app/data/all.php',
    //    method: 'POST'
    //}),
    //reader: {
    //    type: 'json',
    //    root: 'books',
    //    successProperty: 'success'
    //}
});
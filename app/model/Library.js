/**
 * Created by dima on 10/12/14.
 */
Ext.define('BookApp.model.Library', {
    extend: 'Ext.data.Model',
    fields: ["id", "name", "city"]
});
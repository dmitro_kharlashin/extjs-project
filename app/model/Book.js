/**
 * Created by dima on 10/12/14.
 */
Ext.define('BookApp.model.Book', {
    extend: 'Ext.data.Model',
    requires: ['BookApp.model.Library'],
    fields: ['id','name', 'author', 'year', 'library']
});
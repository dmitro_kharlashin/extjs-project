<?php
/**
 * Created by PhpStorm.
 * User: dima
 * Date: 10/12/14
 * Time: 17:58
 */

$books = [
  ["id"=> 1, "name"=> 'Война и мир', "author"=> 'Л. Толстой', "year"=> 1863, "library"=> 1],
  ["id"=> 2, "name"=> 'Отцы и дети', "author"=> 'И. Тургенев', "year"=> 1862, "library"=> 2],
  ["id"=> 3, "name"=> 'Евгений Онегин', "author"=> 'А. Пушкин', "year"=> 1825, "library"=> 2]
];

$library = [
  ["id"=> 1, "name"=> 'Национальная библиотека им. Леси Укранки', "city"=> 'Львов'],
  ["id"=> 2, "name"=> 'Национальная библиотека им. Шевченко', "city"=> 'Киев']
];

//if (isset($_POST['library'])) {
//  foreach ($books as $book) {
//    if ($book['library'] == $_POST['library']) {
//      $books[] = $book;
//    }
//  }
//}

return print json_encode(['success' => true, 'books' => $books, 'library' => $library]);
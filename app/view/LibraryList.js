/**
 * Created by dima on 10/12/14.
 */
Ext.define('BookApp.view.LibraryList', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.librarylist',

    title: 'Библиотека',
    store: 'LibraryStore',

    initComponent: function() {
        this.columns = [
            {header: 'Библиотека', dataIndex: 'name', flex: 2},
            {header: 'Город', dataIndex: 'city', flex: 1}
        ];

        this.callParent(arguments);
    }
});
/**
 * Created by dima on 10/12/14.
 */
Ext.define('BookApp.controller.Libraries', {
    extend: 'Ext.app.Controller',
    requires: ['BookApp.store.BookStore', 'BookApp.store.LibraryStore'],
    views: ['LibraryList'],
    stores: ['LibraryStore', 'BookStore'],
    models: ['Library'],
    init: function() {
        this.control({
            'librarylist': {
                itemdblclick: this.loadLibrary
            }
        });
    },
    loadLibrary: function(grid, record){
        //console.log(grid);
        var me = this;

        var library = record.internalId;

        Ext.Ajax.request({
            url: 'app/data/all.php',
            params: {
                library: library
            },
            success: function (response) {
                var data = Ext.decode(response.responseText);
                if (data.success){
                    //localStorage["books"] = JSON.stringify(data.books);
                    //console.log(data.books[0].id);
                    //localStorage['books']['id'] = data.books[0].id;
                    //localStorage['books']['name'] = data.books[0].id;
                    //localStorage['books']['year'] = data.books[0].id;
                    //localStorage['books']['author'] = data.books[0].id;

                    //var view =Ext.widget('booklist');
                    //view.update(BookApp.view.LibraryList.apply(data.books[0]));
                    var store = Ext.widget('booklist').getStore();
                    store.load();

                    //var view = Ext.widget('booklist');
                    //var table = view.down('grid');
                    //grid.getSelectionModel().on('selectionchange', function(sm, selectedRecord) {
                    //    alert(111);
                    //});

                    Ext.Msg.alert('Обновление');
                //    bookStore = self.getStore('BookStore');
                //    var newBookStore = bookStore.model.setFields(data.books);
                //    bookStore.load({
                //        callback:  function (){
                //            var view =  Ext.widget('booklist');
                //            //var sel_model = view.getSelectionModel();
                //            //var sel = sel_model.getSelection()[1];
                //            //console.log(sel);
                //            //console.log(bookStore.model.getFields());
                //            newBookStore.each(function (record, index) {
                //                var book = self.getModel('Book');
                //                console.log(record);
                //                self.down('booklist').enable();
                //            });
                //        },
                //        scope: self
                //    });
                }
            }
        })
    }
});
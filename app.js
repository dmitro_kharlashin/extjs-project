/**
 * Created by dima on 10/12/14.
 */
Ext.application({
    requires: ['Ext.container.Viewport'],
    name: 'BookApp',
    appFolder: 'app',
    controllers: ['Books', 'Libraries'],

    launch: function() {
        Ext.create('Ext.panel.Panel', {
            renderTo: Ext.getBody(),
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [{
                xtype: 'librarylist',
                itemId: 'librarylist',
                title: 'Библиотека',
                flex: 1
            },{
                xtype: 'booklist',
                itemId: 'booklist',
                title: 'Список книг',
                flex: 2
            }]
        });
    }
});